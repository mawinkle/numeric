#ifndef QUADRATURE_HPP_INCLUDED
#define QUADRATURE_HPP_INCLUDED
#include <vector>
namespace Numeric {
template <typename _Scalar> struct quadrature {
  using Scalar = _Scalar;
  std::vector<Scalar> nodes;
  std::vector<Scalar> weights;
};
} // namespace Numeric
#endif // QUADRATURE_HPP_INCLUDED
