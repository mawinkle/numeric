#ifndef POLYNOMIAL_HPP_INCLUDED
#define POLYNOMIAL_HPP_INCLUDED
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <initializer_list>
#include <type_traits>
namespace Numeric {
	using Eigen::Dynamic;
	static_assert(Dynamic < 0, "Eigen::Dynamic is not negative, this is bad");
	template<typename T, typename R>
	struct same_type{
		constexpr static bool value(){return false;}
		constexpr operator bool(){return false;}
	};
	template<typename T>
	struct same_type<T,T>{
		constexpr static bool value(){return true;}
		constexpr operator bool(){return true;}
	};
	
	template<typename _Scalar, int _len = Dynamic>
	class monomial_basis{
		static_assert(_len >= 0, "Compiletime length must be nonnegative or Eigen::Dynamic");
		public:
			using Scalar = _Scalar;
			constexpr static bool Derivable(){return true;}
		private:
			std::array<Scalar, _len + 1> coefficients;
		public:
		monomial_basis() { std::fill(coefficients.begin(), coefficients.end(), 0); }
		monomial_basis(const std::initializer_list<Scalar>& list) {
			assert(list.size() == _len + 1 && "List size must be equal to compiletime polynomial degree");
			auto it = list.begin();
			for (size_t i = 0; i < list.size(); i++) {
				coefficients[i] = *(it++);
			}
		}
		auto begin() const { return coefficients.begin(); }
		auto end() const { return coefficients.end(); }
		auto rbegin() const { return coefficients.rbegin(); }
		auto rend() const { return coefficients.rend(); }
		auto begin() { return coefficients.begin(); }
		auto end() { return coefficients.end(); }
		auto rbegin() { return coefficients.rbegin(); }
		auto rend() { return coefficients.rend(); }
		auto cbegin() const { return coefficients.cbegin(); }
		auto cend() const { return coefficients.cend(); }
		auto crbegin() const { return coefficients.crbegin(); }
		auto crend() const { return coefficients.crend(); }
		auto front() {return coefficients.front();}
		auto front()const {return coefficients.front();}
		auto back() {return coefficients.back();}
		auto back()const {return coefficients.back();}
		Scalar& operator[](size_t i) { return coefficients[i]; }
		const Scalar& operator[](size_t i) const { return coefficients[i]; }
		Scalar& at(size_t i) { return coefficients.at(i); }
		const Scalar& at(size_t i) const { return coefficients.at(i); }
		size_t size() const { return coefficients.size(); }
		const Scalar& coefficient(size_t i) const { return coefficients[i]; }

		Matrix<Scalar, Dynamic, 1> evaluate(const std::initializer_list<Scalar>& list) {
			Matrix<Scalar, Dynamic, 1> a(list.size());
			for (auto it = list.begin(); it != list.end(); it++) {
				a((it - list.begin())) = *(it);
			}
			return evaluate(a);
		}

		Matrix<Scalar, Dynamic, 1> evaluate(const Matrix<Scalar, Dynamic, 1>& values) {
			Matrix<Scalar, Dynamic, 1> result(values.rows());
			result.array() = coefficients.back();
			for (size_t i = 1; i < coefficients.size(); i++) {
				result.noalias() = result.cwiseProduct(values);
				result.array() += *(coefficients.rbegin() + i);
			}
			return result;
		}
		friend std::ostream& operator<<(std::ostream& out, const monomial_basis<Scalar, _len>& o) {
			for (auto it = o.coefficients.begin(); it != o.coefficients.end() - 1; it++) {
				out << *it << ", ";
			}
			out << o.coefficients.back();
			return out;
		}
	};
	template <typename _Scalar> 
	class monomial_basis<_Scalar, Dynamic>{
	public:
		using Scalar = _Scalar;
		constexpr static bool Derivable(){return true;}
	private:
		std::vector<Scalar> coefficients;

	public:
		monomial_basis() {}
		monomial_basis(unsigned int deg) : coefficients(deg + 1) {}
		monomial_basis(const std::initializer_list<Scalar>& list) : coefficients(list.begin(), list.end()) {}
		template <typename ForwardIterator>
		monomial_basis(ForwardIterator begin, const ForwardIterator& end) : coefficients(begin, end) {}
		int length() const { return coefficients.size(); }
		int degree() const {
			for (auto it = coefficients.rbegin(); it != coefficients.rend() - 1; it++) {
				if (*(it) != 0)
					return (coefficients.rend() - it) - 1;
			}
			return 0;
		}
		auto begin() const { return coefficients.begin(); }
		auto end() const { return coefficients.end(); }
		auto rbegin() const { return coefficients.rbegin(); }
		auto rend() const { return coefficients.rend(); }
		auto begin() { return coefficients.begin(); }
		auto end() { return coefficients.end(); }
		auto rbegin() { return coefficients.rbegin(); }
		auto rend() { return coefficients.rend(); }
		auto cbegin() const { return coefficients.cbegin(); }
		auto cend() const { return coefficients.cend(); }
		auto crbegin() const { return coefficients.crbegin(); }
		auto crend() const { return coefficients.crend(); }
		auto front() {return coefficients.front();}
		auto front()const {return coefficients.front();}
		auto back() {return coefficients.back();}
		auto back()const {return coefficients.back();}
		Scalar& operator[](size_t i) { return coefficients[i]; }
		const Scalar& operator[](size_t i) const { return coefficients[i]; }
		Scalar& at(size_t i) { return coefficients.at(i); }
		const Scalar& at(size_t i) const { return coefficients.at(i); }
		size_t size() const { return coefficients.size(); }
		/**
		  @return Coefficients in ASCENDING order!
		 */
		Scalar& coefficient(size_t i) { return coefficients[i]; }
		/**
		  @return Coefficients in ASCENDING order!
		 */
		const Scalar& coefficient(size_t i) const { return coefficients[i]; }
		Matrix<Scalar, Dynamic, 1> evaluate(const std::initializer_list<Scalar>& list) {
			Matrix<Scalar, Dynamic, 1> a(list.size());
			for (auto it = list.begin(); it != list.end(); it++) {
				a((it - list.begin())) = *(it);
			}
			return evaluate(a);
		}

		Matrix<Scalar, Dynamic, 1> evaluate(const Matrix<Scalar, Dynamic, 1>& values) {
			Matrix<Scalar, Dynamic, 1> result(values.rows());
			result.array() = coefficients.back();
			for (size_t i = 1; i < coefficients.size(); i++) {
				result.noalias() = result.cwiseProduct(values);
				result.array() += *(coefficients.rbegin() + i);
			}
			return result;
		}
		friend std::ostream& operator<<(std::ostream& out, const monomial_basis<Scalar>& o) {
			for (auto it = o.coefficients.begin(); it != o.coefficients.end() - 1; it++) {
				out << *it << ", ";
			}
			out << o.coefficients.back();
			return out;
		}
	};
	
	
	template <typename _Scalar, typename _Basis = monomial_basis<_Scalar, Dynamic>> class polynomial {
		public:
		using Scalar = _Scalar;
		using Basis = _Basis;

		private:
		Basis coefficients; // Stores coefficients in ascending order

		public:
		polynomial() { std::fill(coefficients.begin(), coefficients.end(), 0); }
		polynomial(const std::initializer_list<Scalar>& list) : coefficients(list) {}
		
		template <typename ForwardIterator> polynomial(ForwardIterator begin, const ForwardIterator& end) : coefficients(begin,end) {}

		int degree() const {
			for (auto it = coefficients.rbegin(); it != coefficients.rend() - 1; it++) {
				if (*(it) != 0)
					return (coefficients.rend() - it) - 1;
			}
			return 0;
		}

		int length() const { return coefficients.size() + 1; }
		/**
		  @return Coefficients in ASCENDING order!
		 */
		Scalar& coefficient(size_t i) { return coefficients[i]; }
		/**
		  @return Coefficients in ASCENDING order!
		 */
		const Scalar& coefficient(size_t i) const { return coefficients[i]; }
		size_t size()const{return coefficients.size();}
		Matrix<Scalar, Dynamic, 1> evaluate(const std::initializer_list<Scalar>& list) {
			//Matrix<Scalar, Dynamic, 1> a(list.size());
			//for (auto it = list.begin(); it != list.end(); it++) {
			//	a((it - list.begin())) = *(it);
			//}
			//return evaluate(a);
			return coefficients.evaluate(list);
		}

		Matrix<Scalar, Dynamic, 1> evaluate(const Matrix<Scalar, Dynamic, 1>& values) {
			/*Matrix<Scalar, Dynamic, 1> result(values.rows());
			result.array() = coefficients.back();
			for (size_t i = 1; i < coefficients.size(); i++) {
				result.noalias() = result.cwiseProduct(values);
				result.array() += *(coefficients.rbegin() + i);
			}
			return result;*/
			return coefficients.evaluate(values);
		}
		Scalar& operator[](size_t i) { return coefficients[i]; }
		const Scalar& operator[](size_t i) const { return coefficients[i]; }
		polynomial<Scalar, Basis> derivative() {
			static_assert(Basis::Derivable(), "Cannot derive non-trivially-derivable polynomials");
			polynomial<Scalar, Basis> ret(coefficients.begin() + 1, coefficients.end());
			for (size_t i = 0; i < ret.size(); i++)
				ret[i] *= (i + 1);
			return ret;
		}
		Matrix<Scalar, Dynamic, 1> derivative(const std::initializer_list<Scalar>& list) {
			Matrix<Scalar, Dynamic, 1> a(list.size());
			for (auto it = list.begin(); it != list.end(); it++) {
				a((it - list.begin())) = *(it);
			}
			return derivative(a);
		}
		Matrix<Scalar, Dynamic, 1> derivative(const Matrix<Scalar, Dynamic, 1>& values) {
			Matrix<Scalar, Dynamic, 1> result(values.rows());
			result.array() = coefficients.back() * (coefficients.size() - 1);
			for (size_t i = 1; i < coefficients.size() - 1; i++) {
				result.noalias() = result.cwiseProduct(values);
				result.array() += *(coefficients.rbegin() + i) * (coefficients.size() - 1 - i);
			}
			return result;
		}
		
		friend std::ostream& operator<<(std::ostream& out, const polynomial<Scalar, Basis>& o) {
			return out << o;
		}
	};

	/*template <typename _Scalar, typename _Basis> class polynomial<_Scalar, Dynamic, _Basis> {
		public:
			using Scalar = _Scalar;
			using Basis = _Basis;
		private:
			Basis coefficients;

		public:
			polynomial() {}
			polynomial(unsigned int deg) : coefficients(deg + 1) {}
			polynomial(const std::initializer_list<Scalar>& list) : coefficients(list.begin(), list.end()) {}
			template <typename ForwardIterator>
				polynomial(ForwardIterator begin, const ForwardIterator& end) : coefficients(begin, end) {}
			int length() const { return coefficients.size(); }
			int degree() const {
				for (auto it = coefficients.rbegin(); it != coefficients.rend() - 1; it++) {
					if (*(it) != 0)
						return (coefficients.rend() - it) - 1;
				}
				return 0;
			}
			auto begin() const { return coefficients.begin(); }
			auto end() const { return coefficients.end(); }
			auto rbegin() const { return coefficients.rbegin(); }
			auto rend() const { return coefficients.rend(); }
			auto begin() { return coefficients.begin(); }
			auto end() { return coefficients.end(); }
			auto rbegin() { return coefficients.rbegin(); }
			auto rend() { return coefficients.rend(); }
			auto cbegin() const { return coefficients.cbegin(); }
			auto cend() const { return coefficients.cend(); }
			auto crbegin() const { return coefficients.crbegin(); }
			auto crend() const { return coefficients.crend(); }
			Scalar& operator[](size_t i) { return coefficients[i]; }
			const Scalar& operator[](size_t i) const { return coefficients[i]; }
			Scalar& at(size_t i) { return coefficients.at(i); }
			const Scalar& at(size_t i) const { return coefficients.at(i); }
			size_t size() const { return coefficients.size(); }
			polynomial& push_back(const Scalar& sc) {
				coefficients.push_back(sc);
				return *this;
			}
			polynomial& push_back(Scalar&& sc) {
				coefficients.push_back(std::move(sc));
				return *this;
			}
			polynomial<Scalar> derivative() {
				polynomial<Scalar> ret(coefficients.begin() + 1, coefficients.end());
				for (size_t i = 0; i < ret.size(); i++)
					ret[i] *= (i + 1);
				return ret;
			}
			Matrix<Scalar, Dynamic, 1> derivative(const std::initializer_list<Scalar>& list) {
				Matrix<Scalar, Dynamic, 1> a(list.size());
				for (auto it = list.begin(); it != list.end(); it++) {
					a((it - list.begin())) = *(it);
				}
				return derivative(a);
			}
			Matrix<Scalar, Dynamic, 1> derivative(const Matrix<Scalar, Dynamic, 1>& values) {
				Matrix<Scalar, Dynamic, 1> result(values.rows());
				result.array() = coefficients.back() * (coefficients.size() - 1);
				for (size_t i = 1; i < coefficients.size() - 1; i++) {
					result.noalias() = result.cwiseProduct(values);
					result.array() += *(coefficients.rbegin() + i) * (coefficients.size() - 1 - i);
				}
				return result;
			}
			/
			  @return Coefficients in ASCENDING order!
			 *
			Scalar& coefficient(size_t i) { return coefficients[i]; }
			/
			  @return Coefficients in ASCENDING order!
			 *
			const Scalar& coefficient(size_t i) const { return coefficients[i]; }
			Matrix<Scalar, Dynamic, 1> evaluate(const std::initializer_list<Scalar>& list) {
				/Matrix<Scalar, Dynamic, 1> a(list.size());
				for (auto it = list.begin(); it != list.end(); it++) {
					a((it - list.begin())) = *(it);
				}
				return evaluate(a);*
				return coefficients.evaluate(list);
			}

			Matrix<Scalar, Dynamic, 1> evaluate(const Matrix<Scalar, Dynamic, 1>& values) {
				/Matrix<Scalar, Dynamic, 1> result(values.rows());
				result.array() = coefficients.back();
				for (size_t i = 1; i < coefficients.size(); i++) {
					result.noalias() = result.cwiseProduct(values);
					result.array() += *(coefficients.rbegin() + i);
				}
				return result;*
				return coefficients.evaluate(values);
			}
			friend std::ostream& operator<<(std::ostream& out, const polynomial<Scalar>& o) {
				for (auto it = o.coefficients.begin(); it != o.coefficients.end() - 1; it++) {
					out << *it << ", ";
				}
				out << o.coefficients.back();
				return out;
			}*/
	//};
	inline constexpr int factorial(int i) {
		int r = 1;
		for (int k = 2; k <= i; k++) {
			r *= k;
		}
		return r;
	}
	inline constexpr int minus_one_pow(int k) {
		if (k & 1)
			return -1;
		return 1;
	}
	template <typename Scalar, int size = Dynamic> polynomial<Scalar, monomial_basis<Scalar, size>> legendre() {
		static_assert(size >= 0, "Compiletime degree must be nonnegative or Eigen::Dynamic");
		polynomial<Scalar, monomial_basis<Scalar, size>> poly;
		for (int k = 0; k <= size / 2; k++) {
			Scalar coeff = (Scalar(factorial(2 * size - 2 * k)) * minus_one_pow(k)) / factorial(size - k) /
				factorial(size - 2 * k) / factorial(k) / std::pow(Scalar(2), size);
			poly.coefficient(size - 2 * k) = std::move(coeff);
		}
		return poly;
	}
	template <typename Scalar> polynomial<Scalar, monomial_basis<Scalar, Dynamic>> legendre(int size) {
		assert(size >= 0 && "Size must be nonnegative");
		polynomial<Scalar, monomial_basis<Scalar, Dynamic>> poly(size);
		for (int k = 0; k <= size / 2; k++) {
			Scalar coeff = (Scalar(factorial(2 * size - 2 * k)) * minus_one_pow(k)) / factorial(size - k) /
				factorial(size - 2 * k) / factorial(k) / std::pow(Scalar(2), size);
			poly.coefficient(size - 2 * k) = std::move(coeff);
		}
		return poly;
	}
} // namespace Numeric
#endif // POLYNOMIAL_HPP_INCLUDED
