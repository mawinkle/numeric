#! /usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np

x, y = np.loadtxt('data.txt', delimiter=',', unpack=True)
plt.plot(x,y, label='Rk4')
#plt.plot(x,z, label='Explicit Euler')
#plt.plot(x,true, label='Exact solution')
plt.ylim(-10,10)
#asd = np.linspace(0.0,x[-1],num=10000)
#plt.plot(asd,2 - np.exp(-4 * asd), label='Solution')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Pl0t')
plt.legend()
plt.show()

