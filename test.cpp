#include "explicit_ode.hpp"
#include "polynomial.hpp"
#include "cubic_spline.hpp"
#include <Eigen/Dense>
#include <typeinfo>
#include <fstream>
/*#define MatrixXd MatrixXf
#define VectorXd VectorXf
#define double float*/
template<int n>
using VectorD = Eigen::Matrix<double, -1, n>;
using namespace Eigen;
int main(){
	VectorXd a(5);
	a << 1,2,3,4,5;
	VectorXd b(5);
	b << -3,3,7,6,10;
	Numeric::cubic_spline<double> spline(a,b);
  std::ofstream file("data.txt");
  size_t step = 0;
  for (double x = 1; x <= 4.9; x += 0.1) {
    file << step++ << ", " << spline(x) << "\n";
  }
  return 0;
}
int mian() {

  Numeric::polynomial<double> poly = {100,2,1,2};
  //std::cout << poly.derivative().evaluate({4}) << std::endl;
  MatrixXd A(2, 2);
  A << 0,-1, 1, 0;
  auto func = [&A](double t, Vector2d y) -> Vector2d {
    (void)t;
    return Vector2d(A * y);
    std::swap(y(0), y(1));
    y(0) = -y(0);
    return y;
  };
  Vector2d init(2);
  init << 1, 0;
  const double stepsize = 1;
  const double upto = 120;
  constexpr double ratio = 1;
  std::vector<Vector2d> sol =  Numeric::solveExplicit(func, Numeric::rk4(), {0.0, upto}, init, stepsize);
  std::vector<Vector2d> sol2 = Numeric::solveExplicit(func, Numeric::explicit_euler(), {0.0, upto}, init, stepsize / ratio);
  std::vector<Vector2d> sol3 = Numeric::solveExplicit(func, Numeric::rk4(), {0.0, upto + 1}, init, 0.001);
  std::ofstream file("data.txt");
  for (size_t i = 0; i < sol.size(); i++) {
    file << 1.0 * i * stepsize << ", " << sol[i](0) << ", " << sol2[i * ratio](0) << ", "
         << ((sol.size() - i) ? sol3[i * (int)(stepsize / 0.001)](0) : 0.0) << "\n";
  }
  return 0;
}
