#ifndef EXPLICIT_ODE_HPP
#define EXPLICIT_ODE_HPP
#include "butcher.hpp"
#include <utility>
#include <vector>

namespace Numeric {
using Eigen::Dynamic;
template <int size = Dynamic, typename Function, typename Scalar>
std::vector<Matrix<Scalar, size, 1>>
solveExplicit(Function f, const butcher_tableau<Scalar>& tableau, const std::pair<Scalar, Scalar>& interval,
              const Matrix<Scalar, size, 1>& initial_y, const Scalar& h) {
  using scalar_vector = Matrix<Scalar, size, 1>;
  scalar_vector current_y(initial_y);
  std::vector<scalar_vector> solution;
  solution.push_back(initial_y);
  Scalar current_t(interval.first);

  while (current_t <= interval.second) {
    std::vector<scalar_vector> stages_k;

    for (size_t i = 0; i < tableau.stages(); i++) {
      scalar_vector evaluation_y = current_y;
      Scalar evaluation_t = current_t + h * tableau.time_coefficient(i);
      if (i > 0) {
        scalar_vector evaluation_adder = scalar_vector::Zero(evaluation_y.rows());
        for (size_t ih = 0; ih < i; ih++) {
          evaluation_adder.noalias() += stages_k[ih] * tableau.coefficient(i, ih);
        }
        evaluation_y.noalias() += h * evaluation_adder;
      }
      stages_k.push_back(f(std::move(evaluation_t), std::move(evaluation_y)));
    }

    current_t += h;

    for (size_t i = 0; i < tableau.stages(); i++) {
      current_y += h * tableau.weight(i) * stages_k[i];
    }
    solution.push_back(current_y);
  }
  return solution;
}
} // namespace Numeric

#endif // EXPLICIT_ODE_HPP
