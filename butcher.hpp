#ifndef BUTCHER_HPP_INCLUDED
#define BUTCHER_HPP_INCLUDED
#include <Eigen/Dense>
#include <iostream>
namespace Numeric {
using namespace Eigen;
using std::size_t;
template <typename _Scalar> class butcher_tableau {
public:
  using Scalar = _Scalar;

private:
  size_t coeff_width;
  size_t coeff_height;
  Matrix<Scalar, Dynamic, Dynamic> coefficients;
  Matrix<Scalar, Dynamic, 1> time_coefficients;
  Matrix<Scalar, Dynamic, 1> weights;

public:
  butcher_tableau(const Matrix<Scalar, Dynamic, Dynamic>& everything)
      : coeff_width(everything.cols() - 1), coeff_height(everything.rows() - 1),
        coefficients(everything.block(0, 1, coeff_width, coeff_height)),
        time_coefficients(everything.col(0).head(coeff_height)),
        weights(everything.row(coeff_height).tail(coeff_width).transpose()) {}
  Scalar& coefficient(size_t i, size_t j) noexcept { return coefficients(i, j); }

  Scalar& time_coefficient(size_t j) noexcept { return time_coefficients(j); }
  Scalar& weight(size_t i) noexcept { return weights(i); }
  const Scalar& coefficient(size_t i, size_t j) const noexcept { return coefficients(i, j); }

  const Scalar& time_coefficient(size_t j) const noexcept { return time_coefficients(j); }

  const Scalar& weight(size_t i) const noexcept { return weights(i); }

  size_t stages() const noexcept { return time_coefficients.rows(); }

  bool isConsistent(const Scalar& epsilon = 1e-7) { return std::abs(weights.sum() - 1) <= epsilon; }

  bool isExplicit() {
    for (size_t i = 0; i < coefficients.rows(); i++) {
      for (size_t ih = i; ih < coefficients.cols(); ih++) {
        if (coefficients(i, ih) != 0)
          return false;
      }
    }
    return true;
  }

  bool diagonallyImplicit() {
    for (size_t i = 0; i < coefficients.rows(); i++) {
      for (size_t ih = i + 1; ih < coefficients.cols(); ih++) {
        if (coefficients(i, ih) != 0)
          return false;
      }
    }
    return true;
  }

  void print() {
    ::std::cout << coefficients << "\n";
    ::std::cout << time_coefficients << "\n";
    ::std::cout << weights << "\n";
  }

  int impliciticity() {
    int impliciticity = 0;
    for (size_t i = 0; i < coefficients.rows(); i++) {
      for (size_t ih = i; ih < coefficients.cols(); ih++) {
        if (coefficients(i, ih) != 0) {
          if (i == ih)
            impliciticity = 1;
          return 2;
        }
      }
    }
    return impliciticity;
  }
};
inline butcher_tableau<double> rk4() {
  MatrixXd a___(5, 5);
  a___ << 0, 0, 0, 0, 0, 0.5, 0.5, 0, 0, 0, 0.5, 0, 0.5, 0, 0, 1, 0, 0, 1, 0, 0, 1.0 / 6.0, 1.0 / 3.0,
      1.0 / 3.0, 1.0 / 6.0;
  return butcher_tableau<double>(a___);
}
inline butcher_tableau<double> explicit_euler() {
  MatrixXd a___(2, 2);
  a___ << 0, 0, 0, 1;
  return butcher_tableau<double>(a___);
}
inline butcher_tableau<double> rk38() {
  MatrixXd a___(5, 5);
  a___ << 0, 0, 0, 0, 0, 1.0 / 3.0, 1.0 / 3.0, 0, 0, 0, 2.0 / 3.0, -1.0 / 3.0, 1, 0, 0, 1, 1, -1, 1, 0, 0,
      1.0 / 8.0, 3.0 / 8.0, 3.0 / 8.0, 1.0 / 8.0;
  return butcher_tableau<double>(a___);
}
inline butcher_tableau<double> heun() {
  MatrixXd a___(3, 3);
  a___ << 0, 0, 0, 1, 1, 0, 0, 0.5, 0.5;
  return butcher_tableau<double>(a___);
}
} // namespace Numeric
#endif // BUTCHER_HPP_INCLUDED
