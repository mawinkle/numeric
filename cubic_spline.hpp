#ifndef CUBIC_SPLINE_HPP
#define CUBIC_SPLINE_HPP
#include "polynomial.hpp"
#include <vector>
#include <Eigen/Dense>
namespace Numeric{
using Eigen::Matrix;
using Eigen::Dynamic;
template<typename _Scalar, typename _polynomial = polynomial<_Scalar, monomial_basis<_Scalar>>>
struct cubic_spline{
	template<typename _size_type = std::size_t, typename _I_Scalar = _Scalar>
	struct convertible_index_value_pair{
		using I_Scalar = _I_Scalar;
		using size_type = _size_type;
		size_type i;
		I_Scalar val;
		convertible_index_value_pair(size_type&& _i, I_Scalar&& _val) : i(_i), val(_val){}
		operator I_Scalar&&(){
			return std::move(val);
		}
		operator I_Scalar(){
			return val;
		}
	};
	using poly = _polynomial;
	using Scalar = _Scalar;
	std::vector<poly> polynomials;
	Matrix<Scalar, Dynamic, 1> t;
	cubic_spline(const Matrix<Scalar, Dynamic, 1>& T, const Matrix<Scalar, Dynamic, 1>& Y) : t(T){
		int n = T.size() - 1;
		Matrix<Scalar, Dynamic, 1> h = T.tail(n) - T.head(n);
		Matrix<Scalar, Dynamic, Dynamic> A = Matrix<Scalar, Dynamic, Dynamic>::Zero(n-1, n-1);
		A.diagonal()   = (T.segment(2,n - 1) - T.segment(0,n-1))/3;
		A.diagonal(1)  = h.segment(1,n - 2)/6;
		A.diagonal(-1) = h.segment(1,n - 2)/6; 
	
		Matrix<Scalar, Dynamic, 1> slope = (Y.tail(n) - Y.head(n)).cwiseQuotient(h); 
					
		Matrix<Scalar, Dynamic, 1> r = slope.tail(n - 1) - slope.head(n - 1);
	
		Matrix<Scalar, Dynamic, 1> sigma(n+1);
		sigma.segment(1,n - 1) = A.partialPivLu().solve(r);
		sigma(0) = 0;
		sigma(n) = 0;

		Matrix<Scalar, Dynamic, Dynamic> spline(4, n);
		spline.row(0) = Y.head(n);
		spline.row(1) = slope - h.cwiseProduct(2*sigma.head(n) + sigma.tail(n))/6;
		spline.row(2) = sigma.head(n)/2;
		spline.row(3) = (sigma.tail(n) - sigma.head(n)).cwiseQuotient(6 * h);
		polynomials.reserve(n);
		for(unsigned int i = 0;i < spline.cols();i++){
			polynomials.emplace_back(poly({spline(0,i),spline(1,i),spline(2,i),spline(3,i)}));
		}
		t = T;
	}
	Scalar operator()(const Scalar& x, unsigned int a = 0, unsigned int b = -1){
		if(b == -1){
			b = t.size();
		}
		assert(b >= a);
		//std::cout << t(0) << ", " << t(t.rows() - 1) << " <limits\n";
		if(x < t(0) || x > t(t.rows() - 1)){
			throw std::invalid_argument(std::string("x = ") + std::to_string(x) + " not in spline range");
		}
		//for(size_t i = 0;i < t.rows();i++){
		//	if(t(i) > x){
		//		//std::cout << i << std::endl;
		//		//std::cout << polynomials[i - 1].size() << std::endl;
		//		auto res = polynomials[i - 1].evaluate({x - t(i - 1)});
		//		//std::cout << res.rows() << std::endl;
		//		return polynomials[i - 1].evaluate({x - t(i - 1)})(0);
		//	}
		//}
		//throw 4;
		while(true){
			if(b - a <= 4){
				//std::cout << "sfsd" << std::endl;
				for(;a <= b;a++){
					if(t(a) == x){
						return polynomials[a].evaluate({0.0})(0);
					}
					if(t(a) > x){
						//std::cout << a << std::endl;
						return polynomials[a - 1].evaluate({x - t(a - 1)})(0);
					}

				}
			}
			else{
				unsigned int i = ((size_t)b + (size_t)a) / 2;
				if(t(i) > x)b = i;
				else if(t(i) < x)a = i;
				else if(t(i) == x){
					//std::cout << t(i) << " : " << x << std::endl;
					//std::cout << "ddd" << std::endl;
					return polynomials[i].evaluate({0})(0);
				}
			}
		}
		throw 4;
	}
};
}
#endif //CUBIC_SPLINE_HPP
